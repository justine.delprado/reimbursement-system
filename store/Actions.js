export const ACTIONS = {
    NOTIFY: 'NOTIFY',
    AUTH: 'AUTH',
    ADD_REIMBURSEMENT: 'ADD_REIMBURSEMENT',
    ADD_MODAL: 'ADD_MODAL',
    ADD_ORDERS: 'ADD_ORDERS',
    ADD_USERS: 'ADD_USERS'
}

export const addToReimbursement = (reimbursement) => {//for add to cart of item
    if(!check) return ({ type: 'NOTIFY', payload: {error: 'The request has been added.'} }) //for add to cart of item
    return ({ type: 'ADD_REIMBURSEMENT', payload: [...reimbursement] }) //for add to cart of item
}//for add to cart of item

export const deleteItem = (data, id, type) => {
    const newData = data.filter(item => item._id !== id)
    return ({ type, payload: newData})
}

export const updateItem = (data, id, post, type) => {
    const newData = data.map(item => (item._id === id ? post : item))
    return ({ type, payload: newData})
}