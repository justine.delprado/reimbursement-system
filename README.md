# reimbursement-system

Pano I-RUN

```bash
1. iextract here mo yung Extract me.rar
2. para maparun mo yung website npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


## FRONTEND FRONTEND NOTICE FRONTEND NOTICE

When moving the frontend pages to pages folder

- copy format ng auditTrail.js , palitan ano dapat palitan na name
- change class to className
- strictly bootstrap format ang class
- pagpinasok sya sa js nakapaloob sya sa "<div></div> or <main></main>"basta ang return isang buo parang "<body></body>"
- if may trip talaga na style ganto format style={{marginLeft: '-230px'}} , basta ang alam ko di sya natanggap ng may mga dash(e.g margin-Left) so search nalang ng mga format.
  -always end everything with </example>. yung input format ng end nya nasa loob nalang <input examples/>

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
