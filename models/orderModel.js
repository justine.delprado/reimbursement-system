import mongoose from 'mongoose'

const orderSchema = new mongoose.Schema({
    user: {
        type: mongoose.Types.ObjectId,
        ref: 'user'
    },
    expense: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    totalExpense: {
        type: Number,
        default: 0
    },
    images: {
        type: Array,
        required: true
    },
    statusMAN: {
        type: Boolean,
        default: false
    },
    statusHR: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: true
})

let Dataset = mongoose.models.order || mongoose.model('order', orderSchema)
export default Dataset