import mongoose from 'mongoose'

const reimbursementSchema = new mongoose.Schema({
    user: {
        type: mongoose.Types.ObjectId,
        ref: 'user'
    },
    expense: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    total: {
        type: String,
        required: true,
    },
    images: {
        type: Array,
        required: true
    },
    statushr: {
        type: Boolean,
        default: false
    },
    statusmg: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: true
})

let Dataset = mongoose.models.reimbursement || mongoose.model('reimbursement', reimbursementSchema)
export default Dataset