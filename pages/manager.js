import Link from 'next/link'
import Head from 'next/head'
import { useContext } from 'react'
import { DataContext } from '../store/GlobalState'
import { postData, getData, putData } from '../utils/fetchData'//for to create table in mongodb

const manager = () => {
    const { state } = useContext(DataContext)
    const { orders } = state
    return(
        <div>
                <center><div style={{fontSize: '25px'}}>MANAGER DASHBOARD</div></center>
        <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Employee No.</th>
                <th>Submitted Time</th>
                <th>Employee Name</th>
                <th>Email</th>
                <th>Update</th>
                <th colspan="2"> Status</th>
            </tr>
        </thead>
        <tbody>
        {
            orders.map((order, index) => (
                <tr key={order._id}>
                    <td style={{paddingLeft: '10px', paddingRight: '10px'}}>
                        {index + 1}
                    </td>
                    
                    <td>
                        {new Date(order.createdAt).toLocaleDateString()}
                    </td>

                    <td>
                        <span>{order.user.name}</span>
                    </td>

                    <td>
                        <span>{order.user.email}</span>
                    </td>
                    
                    <td>
                        <Link href={`/order/${order._id}`} className="btn btn-danger w-20"  style={{color:'white'}}>
                            Update
                        </Link>&nbsp;&nbsp;
                    </td>

                    <td>
                        {
                            order.statusMAN
                            ? <span className="fas fa-check text-success"> Approved</span>
                            : <span className="fas fa-times text-danger"> Pending</span>
                        }
                    </td>
                </tr> 
            ))
        }
        </tbody>
    </table>

    
    </div>
        
    )
}

export default manager