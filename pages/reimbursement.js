import Link from 'next/link' //for link
import Head from 'next/head' //for Head of HTML
import { useRouter } from 'next/router'//for automatic change the pages after creating order
import { useContext, useState, useEffect } from 'react'//for cart and cart empty and computation
import { DataContext } from '../store/GlobalState'//for cart and cart empty
import {imageUpload} from '../utils/imageUpload'
import { postData } from '../utils/fetchData'//for to create table in mongodb

const Cart = () => {


  const { state, dispatch } = useContext(DataContext)//for cart and cart empty
  const { auth, orders } = state //for cart and cart empty
  const [expense, setExpense] = useState('')//for data
  const [date, setDate] = useState('')//for data
  const [totalExpense, setTotalExpense] = useState('')//for data
  const router = useRouter()

// --------------------------

const [images, setImages] = useState([])

const uploadImages = e => {
  dispatch({type: 'NOTIFY', payload: {}})
  let newImages = []
  let num = 0
  let err = ''
  const files = [...e.target.files]
  if(files.length === 0)
  return dispatch({type: 'NOTIFY', payload: {error: 'Files does not exist.'}})
  files.forEach(file => {
      if(file.size > 1024 * 1024)
      return err = 'The largest image size is 1mb'
      if(file.type !== 'image/jpeg' && file.type !== 'image/png')
      return err = 'Image format is incorrect.'
      num += 1;
      if(num <= 4) newImages.push(file)
      return newImages;
  })
  if(err) dispatch({type: 'NOTIFY', payload: {error: err}})
  const imgCount = images.length
  if(imgCount + newImages.length > 4)
  return dispatch({type: 'NOTIFY', payload: {error: 'Select up to 4 images.'}})
  setImages([...images, ...newImages])
}

const deleteImage = index => {
  const newArr = [...images]
  newArr.splice(index, 1)
  setImages(newArr)
}

// --------------------------

  const handlePayment = async () => {//for validations
    if(!expense)//for validations
    return dispatch({ type: 'NOTIFY', payload: {error: 'Please choose your expense type.'}})//for validations
    if(expense === 'all')
    return dispatch({type: 'NOTIFY', payload: {error: 'Please choose expense type.'}})
    if(!date)//for validations
    return dispatch({ type: 'NOTIFY', payload: {error: 'Please add date.'}})//for validations
    if(!totalExpense)//for validations
    return dispatch({ type: 'NOTIFY', payload: {error: 'Please add your total expense.'}})//for validations
    if(images.length === 0)//for validations
    return dispatch({ type: 'NOTIFY', payload: {error: 'Please add your receipt.'}})//for validations

    dispatch({ type: 'NOTIFY', payload: {loading: true} })//for creating order

    let media = []//images
    const imgNewURL = images.filter(img => !img.url)//images
    const imgOldURL = images.filter(img => img.url)//images
  
    if(imgNewURL.length > 0) media = await imageUpload(imgNewURL)//images

    postData('order', {expense, date, totalExpense, images: [...imgOldURL, ...media]}, auth.token)//for creating order
    .then(res => {//for creating order
      if(res.err) return dispatch({ type: 'NOTIFY', payload: {error: res.err} })//for creating order
      const newOrder = {//for creating order
        ...res.newOrder,//for creating order
        user: auth.user//for creating order
      }//for creating order
      dispatch({ type: 'ADD_ORDERS', payload: [...orders, newOrder] })//for creating order
      dispatch({ type: 'NOTIFY', payload: {success: res.msg} })//for creating order
      return router.push(`/order/${res.newOrder._id}`)//for creating order and automatic change the pages after creating order
    })//for creating order
  }
    // user data

    const [data, setData] = useState('')
    const { _id, name, email, contact} = data


    useEffect(() => {
        if(auth.user) setData({...data, name: auth.user.name, email: auth.user.email, contact: auth.user.contact, _id: auth.user._id})
    },[auth.user])

    if(!auth.user) return null;

    // ------

    //HTML
    return(
      <div >
      <Head>
        <title>Reimbursement</title>
      </Head>

      <form className="form-area">
        <div className="form-box" style={{marginTop: '75px', marginBottom: '75px'}}>
          <h2 className="h22">
            <center>
              REIMBURSEMENT FORM
            </center>
          </h2> 

          <div className="form-group">
            <div className="row">
              <div className="col">
                  <div className="form-group">
                      <label htmlFor="exampleInputEmnum1">Employee Number</label>
                      <input defaultValue={_id} className="form-control" readOnly={true}  />
                      
                      <label htmlFor="name">Name</label>
                      <input defaultValue={name} className="form-control" readOnly={true}  />

                      <label htmlFor="exampleInputEmail1">Email</label>
                      <input defaultValue={email} className="form-control" readOnly={true} />

                      <label htmlFor="exampleInputContact">Contact Number</label>
                      <input defaultValue={contact} className="form-control" readOnly={true} />
                  </div>
                  <label className="form-check-label" htmlFor="exampleCheck1" style={{fontSize: '128%', textAlign: 'justify'}}>
                    I certify that the above facts are true to the best of my knowledge and belief and I understand that I subject myself to disciplinary action in the event that the above facts are found to be falsified.
                  </label>
              </div>

              <div className="col">
                <div className="form-group">
                  <label htmlFor="exampleInputPosition">Expense Type</label><br></br>
                    <select className="form-control" aria-label="Default select example" id="expense"
                    name="expense" value={expense}  onChange={e => setExpense(e.target.value)}  >
                      <option value="all" selected>Expenses Type</option>
                      <option value="Meal">Meal</option>
                      <option value="Transport">Transport</option>
                      <option value="Hotel">Hotel</option>
                    </select>

                    <label htmlFor="date" style={{marginTop: '10px'}}>Date</label>
                    <input type="date" name="date" id="date"
                    className="form-control" value={date}
                    onChange={e => setDate(e.target.value)} />

                    <label htmlFor="totalExpense">Total Expense</label>
                    <input type="text" name="totalExpense" id="totalExpense"
                    className="form-control" value={totalExpense}
                    onChange={e => setTotalExpense(e.target.value)} />

                    <label htmlFor="exampleInputName1">Receipt</label>
                    <div className="input-group mb-3">
                      <div className="input-group-prepend">
                          <span className="input-group-text">Upload</span>
                      </div>
                      <div className="custom-file border rounded">
                        <input type="file" className="custom-file-input"
                        accept="image/*" onChange={uploadImages} multiple accept="image/*"/>
                      </div>
                    </div> 

                    <div className="row img-up mx-0">
                      {
                        images.map((img, index) => (
                        <div key={index} className="file_img my-1">
                            <img src={img.url ? img.url : URL.createObjectURL(img)}
                            alt="" className="img-thumbnail rounded" />

                            <span onClick={() => deleteImage(index)}>X</span>
                        </div>
                        ))
                      }
                    </div>  
                  </div>
                </div>
              </div>

              <center>
                <Link href={auth.user ? '#!' : '/signin'}>
                  <a className="btn btn-dark my-2 w-100" onClick={handlePayment}>Submit for Approval</a>
                </Link>
              </center>
            </div>
          </div>
        </form>
      </div>

    )
  }
  
export default Cart