import Head from 'next/head'
import { useState, useContext, useEffect } from 'react'
import { DataContext } from '../store/GlobalState'
import valid from '../utils/valid'
import { patchData } from '../utils/fetchData'
import {imageUpload} from '../utils/imageUpload'

const Profile = () => {
    const initialState = {
        avatar: '',
        name: '',
        contact: '',
        password: '',
        cf_password: ''
    }
    const [data, setData] = useState(initialState)
    const { avatar, name, contact, password, cf_password } = data

    const {state, dispatch} = useContext(DataContext)
    const { auth, notify } = state

    useEffect(() => {
        if(auth.user) setData({...data, name: auth.user.name, contact: auth.user.contact})
    },[auth.user])

    const handleChange = (e) => {
        const { name, contact, value } = e.target
        setData({...data, [name]:value, [contact]:value})
        dispatch({ type: 'NOTIFY', payload: {} })
    }

    const handleUpdateProfile = e => {
        e.preventDefault()
        if(password){
            const errMsg = valid(name, contact, auth.user.email, password, cf_password)
            if(errMsg) return dispatch({ type: 'NOTIFY', payload: {error: errMsg} })
            updatePassword()
        }

        if(name !== auth.user.name || avatar) updateInfor()
    }

    const updatePassword = () => {
        dispatch({ type: 'NOTIFY', payload: {loading: true} })
        patchData('user/resetPassword', {password}, auth.token)
        .then(res => {
            if(res.err) return dispatch({ type: 'NOTIFY', payload: {error: res.err} })
            return dispatch({ type: 'NOTIFY', payload: {success: res.msg} })
        })
    }

    const changeAvatar = (e) => {
        const file = e.target.files[0]
        if(!file)
            return dispatch({type: 'NOTIFY', payload: {error: 'File does not exist.'}})

        if(file.size > 1024 * 1024) //1mb
            return dispatch({type: 'NOTIFY', payload: {error: 'The largest image size is 1mb.'}})

        if(file.type !== "image/jpeg" && file.type !== "image/png") //1mb
            return dispatch({type: 'NOTIFY', payload: {error: 'Image format is incorrect.'}})
        
        setData({...data, avatar: file})
    }

    const updateInfor = async () => {
        let media;
        dispatch({type: 'NOTIFY', payload: {loading: true}})

        if(avatar) media = await imageUpload([avatar])

        patchData('user', {
            name, contact, avatar: avatar ? media[0].url : auth.user.avatar
        }, auth.token).then(res => {
            if(res.err) return dispatch({type: 'NOTIFY', payload: {error: res.err}})

            dispatch({type: 'AUTH', payload: {
                token: auth.token,
                user: res.user
            }})
            return dispatch({type: 'NOTIFY', payload: {success: res.msg}})
        })
    }

    if(!auth.user) return null;
    return( 
        <div className="box_1">
            <section className="stay"> 
                <p>
                    One of the things that is hindering the globalization of Filipino businesses is the <br></br>
                    inaccessibility of technology to promote innovation.  LegionTech Inc. is committed <br></br>
                    to enable Filipino businesses become unstoppable for global competitiveness through <br></br>
                    technology and innovation. One of the things that is hindering the globalization of Filipino businesses is the <br></br>
                    inaccessibility of technology to promote innovation.  LegionTech Inc. is committed <br></br>
                    he things that is hindering the globalization of Filipino businesses is the <br></br>
                    inaccessibility of technology to promote innovation.  LegionTech Inc. is committed <br></br>
                    to enable Filipino businesses become unstoppable for global competitiveness through <br></br>
                    technology and innovation.
                </p>
            </section>
        
            <div className="profile_page"  style={{marginLeft: '25%', marginRight: '25%'}}>
                <Head>
                    <title>Profile</title>
                </Head>
            <div className="box_1">
                <section className="text-secondary my-3">
                    <h3 className="text-center text-uppercase">
                        {auth.user.role === 'user' ? 'User Profile' : 'Admin Profile'}
                    </h3>

                    <div className="avatar">
                        <img src={avatar ? URL.createObjectURL(avatar) : auth.user.avatar} 
                        alt="avatar" />
                        <span>
                            <i className="fas fa-camera"></i>
                            <p>Change</p>
                            <input type="file" name="file" id="file_up"
                            accept="image/*" onChange={changeAvatar} />
                        </span>
                    </div>

                    <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input type="text" name="name" value={name} className="form-control"
                        onChange={handleChange}  />

                        <label htmlFor="contact">Contact</label>
                        <input type="text" name="contact" value={contact} className="form-control"
                        onChange={handleChange}  />
                        
                        <label htmlFor="email">Email</label>
                        <input type="text" name="email" defaultValue={auth.user.email}
                        className="form-control" disabled={true} />
                    </div>
                    
                    <div className="form-group">
                        <label htmlFor="password">New Password</label>
                        <input type="password" name="password" value={password} className="form-control"
                        placeholder="Your new password" onChange={handleChange}  />

                        <label htmlFor="cf_password">Confirm New Password</label>
                        <input type="password" name="cf_password" value={cf_password} className="form-control"
                        placeholder="Confirm new password" onChange={handleChange} />
                    </div>

                    <div className="form-group">
                        <button  style={{backgroundColor:'green', width: '100%'}} className="btn btn-info" disabled={notify.loading}
                        onClick={handleUpdateProfile}>
                            Update
                        </button>
                    </div>
                </section>
                </div>
            </div>
        </div>
    )
}

export default Profile