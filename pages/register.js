import Head from 'next/head'
import Link from 'next/link'
import { useState, useContext, useEffect } from 'react'
import valid from '../utils/valid'
import { DataContext } from '../store/GlobalState'
import { postData } from '../utils/fetchData'
import { useRouter } from 'next/router'


const Register = () => {
    const initialState = { name: '', email: '', password: '', cf_password: '', contact: '', position: ''}
    const [userData, setUserData] = useState(initialState)
    const { name, email, password, cf_password , contact, position} = userData

    const {state, dispatch} = useContext(DataContext)
    const { auth } = state

    const router = useRouter()

    const handleChangeInput = e => {
        const {name, value} = e.target
        setUserData({...userData, [name]:value})
        dispatch({ type: 'NOTIFY', payload: {} })
    }

    const handleSubmit = async e => {
        e.preventDefault()
        const errMsg = valid(name, email, password, cf_password, contact, position)
        if(errMsg) return dispatch({ type: 'NOTIFY', payload: {error: errMsg} })

        if(position === 'all')
        return dispatch({type: 'NOTIFY', payload: {error: 'Please choose position.'}})

        dispatch({ type: 'NOTIFY', payload: {loading: true} })

        const res = await postData('auth/register', userData)
        
        if(res.err) return dispatch({ type: 'NOTIFY', payload: {error: res.err} })

        dispatch({ type: 'NOTIFY', payload: {success: res.msg} })
        return router.push(`/signin`)
    }

    useEffect(() => {
        if(Object.keys(auth).length !== 0) router.push("/")
    }, [auth])

    return(
        <div className="regbody">
            <Head>
                <title>Register Page</title>
            </Head>
            <div className="regform" style={{marginBottom: '45px', paddingTop: '61px', paddingBottom: '61px'}}>
                <center><h2 className="h223">REGISTER</h2></center>
                <form className="mx-auto my-4" style={{maxWidth: '500px'}} onSubmit={handleSubmit}>
                    <div className="row">
                        <div className="col" style={{marginBottom: '10px'}}>
                            <div className="form-group">
                                <label htmlFor="exampleInputName1">Name</label>
                                <input type="name" className="form-control" id="exampleInputName1" 
                                name="name" value={name} onChange={handleChangeInput} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="exampleInputContact">Contact Number</label>
                                <input type="contact" className="form-control" id="exampleInputContact"
                                name="contact" value={contact} onChange={handleChangeInput} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="exampleInputPosition">Position</label><br></br>
                                <select className="form-control" aria-label="Default select example" id="exampleInputPosition"
                                name="position" value={position} onChange={handleChangeInput} >
                                    <option value="all" selected>Position Type</option>
                                    <option value="employee">Employee</option>
                                    <option value="manager">Manager</option>
                                    <option value="hr">HR</option>
                                </select>
                            </div>
                        </div>

                        <div className="col" style={{marginBottom: '10px'}}>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Email</label>
                                <div className="input-group mb-3">
                                    <input type="email" className="form-control" id="exampleInputEmail1"
                                    name="email" value={email} onChange={handleChangeInput} />
                                </div>
                            </div>

                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Password</label>
                                <input type="password" className="form-control" id="exampleInputPassword1"
                                name="password" value={password} onChange={handleChangeInput} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="exampleInputPassword2">Confirm Password</label>
                                <input type="password" className="form-control" id="exampleInputPassword2"
                                name="cf_password" value={cf_password} onChange={handleChangeInput} />
                            </div>
                        </div>
                    </div>

                    <button type="submit" className="btn btn-dark w-100">Register</button>

                    <p className="my-2">
                        Already have an account? <Link href="/signin"><a style={{color: 'crimson'}}>Login Now</a></Link>
                    </p>
                </form>
            </div>
        </div>
    )
}

export default Register
