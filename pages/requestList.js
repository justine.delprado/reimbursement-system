import Head from 'next/head'
import { useContext } from 'react'
import { DataContext } from '../store/GlobalState'
import Link from 'next/link'

const requestList = () => {
    const { state } = useContext(DataContext)
    const { orders } = state

    return(
        <div>
            <section className="stay"> 
                <p>
                    One of the things that is hindering the globalization of Filipino businesses is the <br></br>
                    inaccessibility of technology to promote innovation.  LegionTech Inc. is committed <br></br>
                    to enable Filipino businesses become unstoppable for global competitiveness through <br></br>
                    technology and innovation. One of the things that is hindering the globalization of Filipino businesses is the <br></br>
                    inaccessibility of technology to promote innovation.  LegionTech Inc. is committed <br></br>
                    he things that is hindering the globalization of Filipino businesses is the <br></br>
                    inaccessibility of technology to promote innovation.  LegionTech Inc. is committed <br></br>
                    to enable Filipino businesses become unstoppable for global competitiveness through <br></br>
                    technology and innovation.
                </p>
            </section>

            <div className="auditbody" style={{marginLeft: '5%', marginRight: '5%', marginBottom: '168px'}}>
                <Head>
                    <title>Request List</title>
                </Head>
                
                <section className="row text-secondary my-3">
                    <div className="col">
                        <center><strong><h2 className="titulo">REQUEST LIST</h2></strong></center>

                        <div className="my-3 table-responsive" style={{textAlign: 'center'}}>
                            <table className="table-bordered table-striped table-hover w-100 text-uppercase">
                                <thead>
                                    <tr>
                                        <th>Request #</th>
                                        <th>Submitted On</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Status Manager</th>
                                        <th>Status HR</th>
                                        <th>Details</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {
                                        orders.map((order, index) => (
                                            <tr key={order._id}>
                                                <td style={{paddingLeft: '10px', paddingRight: '10px'}}>
                                                    {index + 1}
                                                </td>
                                                
                                                <td>
                                                    {new Date(order.createdAt).toLocaleDateString()}
                                                </td>

                                                <td>
                                                    <span>{order.user.name}</span>
                                                </td>

                                                <td>
                                                    <span>{order.user.email}</span>
                                                </td>

                                                <td>
                                                    <span>{order.user.contact}</span>
                                                </td>

                                                <td>
                                                {
                                                        order.statusMAN
                                                        ? <span className="fas fa-check text-success"></span>
                                                        : <span className="fas fa-times text-danger"></span>
                                                    }
                                                </td>

                                                <td>
                                                    {
                                                        order.statusHR
                                                        ? <span className="fas fa-check text-success"></span>
                                                        : <span className="fas fa-times text-danger"></span>
                                                    }
                                                </td>

                                                <td>
                                                    <Link href={`/order/${order._id}`}>
                                                        Details
                                                    </Link>
                                                </td>

                                            </tr> 
                                        ))
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    )
}

export default requestList