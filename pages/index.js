const Home = () => {
  return(
    <div className="indexbody">

      <header className="bgimg-1 w3-display-container w3-grayscale-min" id="home">
        <div className="homepage">
          <p> 
            One of the things that is hindering the globalization of Filipino businesses is the <br></br>
            inaccessibility of technology to promote innovation.  LegionTech Inc. is committed <br></br>
            to enable Filipino businesses become unstoppable for global competitiveness through <br></br>
            technology and innovation.
          </p>
        </div>

        <div className="w3-display-bottomleft w3-text-grey w3-large ml-5">
          <i className="fa fa-facebook-official w3-hover-opacity mr-2" aria-hidden="true"></i>
          <i className="fa fa-instagram w3-hover-opacity mr-2" aria-hidden="true"></i>
          <i className="fa fa-twitter w3-hover-opacity mr-2" aria-hidden="true"></i>
          <i className="fa fa-linkedin w3-hover-opacity mr-2" aria-hidden="true"></i>
        </div>
      </header>

      <div className="about w3-container" id="about" style={{paddingTop: '120px', paddingBottom: '120px'}}>
        <h3 className="text-center">ABOUT THE COMPANY</h3>
        <p className="text-center w3-large">Key features of our company</p>
        <div className="row" style={{marginTop: '64px'}}>
          <div className="col">
            <i className="fa fa-desktop w3-margin-bottom w3-jumbo w3-center" aria-hidden="true"></i>
            <p className="text-large">IT-Enabled</p>
            <p>LegionTech Inc. is committed to bringing current technology and services closer to Filipino businesses without compromise.</p>
          </div>
          <div className="col">
            <i className="fa fa-heart w3-margin-bottom w3-jumbo" aria-hidden="true"></i>
            <p className="text-large">Service</p>
            <p>Legiontech offers wide array of capability development trainings and certifications that are industry recognized to provide value to organizations at multiple levels and areas.</p>
          </div>
          <div className="col">
            <i className="fa fa-diamond w3-margin-bottom w3-jumbo" aria-hidden="true"></i>
            <p className="text-large">Commit</p>
            <p>LegionTech Inc. is committed to contribute to the maturity of information technology in the Philippines.</p>
          </div>
          <div className="col">
            <i className="fa fa-cog w3-margin-bottom w3-jumbo" aria-hidden="true"></i>
            <p className="text-large">Support</p>
            <p>LegionTech Inc. is committed to contribute to the maturity of information technology in the Philippines.</p>
          </div>
        </div>
      </div>

      <div>
        <div className="container">
          <div className="row py-4 d-flex align-items-center">
            <div className="col-md-6 col-lg-7 text-center" style={{marginLeft: '22%'}}>
              <a className="fb-ic">
                  <i className="fab fa-facebook-f white-text mr-4 fa-2x" aria-hidden="true"> </i>
              </a>
              <a className="ins-ic">
                  <i className="fab fa-instagram white-text mr-4 fa-2x" aria-hidden="true"> </i>
              </a>
              <a className="tw-ic">
                  <i className="fab fa-twitter white-text mr-4 fa-2x" aria-hidden="true"> </i>
              </a>
              <a className="li-ic">
                  <i className="fab fa-linkedin-in white-text mr-4 fa-2x" aria-hidden="true"> </i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Home