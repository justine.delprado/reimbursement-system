import connectDB from '../../../../utils/connectDB'
import Orders from '../../../../models/orderModel'
import auth from '../../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch(req.method){
        case "PATCH":
            await deliveredOrder(req, res)
            break;
    }
}

const deliveredOrder = async(req, res) => {
    try {
        const result = await auth(req, res)
        if(result.role !== 'user')
        return res.status(400).json({err: 'Authentication is not valid.'})
        const {id} = req.query


        const order = await Orders.findOne({_id: id})
        if(order){
            await Orders.findOneAndUpdate({_id: id}, {
                statusMAN: true
            })
    
            res.json({
                msg: 'Updated success!',
                result: {
                    statusMAN: true
                }
            })
        }
        
    } catch (err) {
        return res.status(500).json({err: err.message})
    }
}