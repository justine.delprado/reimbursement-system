import connectDB from '../../../utils/connectDB'
import Orders from '../../../models/orderModel'
import auth from '../../../middleware/auth'

connectDB()

export default async (req, res) => {
    switch(req.method){
        case "POST":
            await createOrder(req, res)
            break;
        case "GET":
            await getOrders(req, res)
            break;
    }
}

const getOrders = async (req, res) => {
    try {
        const result = await auth(req, res)

        let orders;
        orders = await Orders.find().populate("user", "-password")

        res.json({orders})
    } catch (err) {
        return res.status(500).json({err: err.message})
    }
}

const createOrder = async (req, res) => {
    try {
        const result = await auth(req, res)
        const { expense, date, totalExpense, images } = req.body

        const newOrder = new Orders({
            user: result.id, expense, date, totalExpense, images
        })

        await newOrder.save()

        res.json({
            msg: 'Request success!',
            newOrder
        })

    } catch (err) {
        return res.status(500).json({err: err.message})
    }
}