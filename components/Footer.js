import React from "react";

function Footer() {
  return (
    <footer className="page-footer font-small blue-grey lighten-5" 
    style={{position: 'relative', bottom: '0', width: '100%', backgroundColor:'lightsteelblue', color:'white', fontSize: '12px'}}>

        <div className="footer-copyright text-center text-black py-3">© 2021 Copyright:
            <a className="text-black" href="http://localhost:3000"> www.legionintegrated.com</a>
        </div>

    </footer>
  );
}

export default Footer;