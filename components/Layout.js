import React from 'react'
import NavBar from './NavBar'
import Notify from './Notify'
import Footer from "./Footer";

function Layout({children}) {
    return (
        <div className="box_1">
            <NavBar />
            <Notify />
            {children}
            <Footer />
        </div>
    )
}

export default Layout
