import { patchData } from '../utils/fetchData'
import { updateItem } from '../store/Actions'
import { useRouter } from 'next/router'
import { useState } from 'react'

const OrderDetail = ({orderDetail, state, dispatch}) => {
    
    const {auth, orders} = state
    const router = useRouter()
    const [tab, setTab] = useState(0)

    const handleDelivered = (order) => {
        dispatch({type: 'NOTIFY', payload: {loading: true}})

        patchData(`order/delivered/${order._id}`, null, auth.token)
        .then(res => {
            if(res.err) return dispatch({type: 'NOTIFY', payload: {error: res.err}})

            const { statusMAN } = res.result

            dispatch(updateItem(orders, order._id, {
                ...order, statusMAN
            }, 'ADD_ORDERS'))

            return dispatch({type: 'NOTIFY', payload: {success: res.msg}})
        })
    }

    const handlePayment = (order) => {
        dispatch({type: 'NOTIFY', payload: {loading: true}})

        patchData(`order/payment/${order._id}`, null, auth.token)
        .then(res => {
            if(res.err) return dispatch({type: 'NOTIFY', payload: {error: res.err}})

            const { statusHR } = res.result

            dispatch(updateItem(orders, order._id, {
                ...order, statusHR
            }, 'ADD_ORDERS'))

            return dispatch({type: 'NOTIFY', payload: {success: res.msg}})
        })
    }
    
    const isActive = (index) => {
        if(tab === index) return " active";
        return ""
    }

    if(!auth.user) return null;
    return(
        <>
        {
            
            orderDetail.map(order => (
                <div style={{height: '100%', width: '100%', backgroundImage: 'linear-gradient(180deg, lightsteelblue, white, lightsteelblue)' }}>
                <div key={order._id} className="row justify-content-around" style={{margin: '20px auto', marginTop: '-50px'}}>
                    <div className="text-uppercase my-3" style={{maxWidth: '650px', marginTop: '-50px'}}>
                        <h2 className="text-break">Request ID: {order._id}</h2>

                        <div className="mt-4 text-secondary">
                            <strong><h3 style={{color: 'black'}}>Employee Details</h3></strong>
                            <p style={{color: 'black'}}>Name: {order.user.name}</p>
                            <p style={{color: 'black'}}>Email: {order.user.email}</p>
                            <p style={{color: 'black'}}>Expense Type: {order.expense}</p>
                            <p style={{color: 'black'}}>Date: {order.date}</p>
                            <p style={{color: 'black'}}>Total Expense: {order.totalExpense}</p>

                            <strong><h3 style={{color: 'black'}}>Receipt</h3></strong>

                            <div className="col">
                                <img className="img-thumbnail rounded center" src={ order.images[tab].url } alt={ order.images[tab].url }                                
                                style={{height: '500px', width: '500px', marginRight: '6%', marginLeft: '6%' }} />

                                <div className="row mx-0" style={{cursor: 'pointer'}} >
                                    {order.images.map((img, index) => (
                                        <img className="img-thumbnail rounded" key={index} src={img.url} alt={img.url}
                                        className={`img-thumbnail rounded ${isActive(index)}`}
                                        style={{width: '120px', height: '97px', marginRight: '2%', marginLeft: '2%', objectFit: 'cover'}}
                                        onClick={() => setTab(index)} />
                                    ))}
                                </div>
                            </div>

                            <h3 style={{color: 'black'}}>Status Manager</h3>
                            <div className={`alert ${order.statusMAN ? 'alert-success' : 'alert-danger'} d-flex justify-content-between align-items-center`} role="alert">
                                {
                                    order.statusMAN ? 'Approved' : 'Pending'
                                }
                                {
                                    auth.user.position === 'manager' && !order.statusMAN &&
                                    <button className="btn btn-info text-uppercase" style={{backgroundColor:'green', color: 'white'}}
                                    onClick={() => handleDelivered(order)}>
                                        Approved
                                    </button>
                                }
                            </div>

                            <h3 className= '' style={{color: 'black'}}>Status HR</h3>
                            <div className={`alert ${order.statusHR ? 'alert-success' : 'alert-danger'} d-flex justify-content-between align-items-center`} role="alert">
                                {
                                    order.statusHR ? 'Approved' : 'Pending'
                                }
                                {
                                    auth.user.position === 'hr' && !order.statusHR &&
                                    <button className="btn btn-info text-uppercase" style={{backgroundColor:'green', color: 'white'}}
                                    onClick={() => handlePayment(order)}>
                                        Approved
                                    </button>
                                }
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            ))
        }
        </>
   
   )
   
}


export default OrderDetail