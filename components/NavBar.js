import Head from 'next/head'
import Link from 'next/link'
import {useState, useContext, useEffect} from 'react'
import {DataContext} from '../store/GlobalState'
import {postData} from '../utils/fetchData'
import Cookie from 'js-cookie'
import { useRouter } from 'next/router'
function NavBar() {
    const router = useRouter()
    const {state, dispatch} = useContext(DataContext)
    const { auth, cart } = state


    const isActive = (r) => {
        if(r === router.pathname){
            return " active"
        }else{
            return ""
        }
    }

    const handleLogout = () => {
        Cookie.remove('refreshtoken', {path: 'api/auth/accessToken'})
        localStorage.removeItem('firstLogin')
        dispatch({ type: 'AUTH', payload: {} })
        dispatch({ type: 'NOTIFY', payload: {success: 'Logged out!'} })
        return router.push('/')
    }

    const adminRouter = () => {
        return(
            <>
            <div className="dropdown-divider"></div>
            <Link href="/users">
                <a className="dropdown-item">Users</a>
            </Link>
            <div className="dropdown-divider"></div>
            <Link href="/register">
                <a className="dropdown-item">Add Users</a>
            </Link>
            </>
        )
    }

    const userRouter = () => {
        return(
            <>
            <div className="dropdown-divider"></div>
            <Link href="/reimbursement">
                <a className="dropdown-item">RB Form</a>
            </Link>
            <div className="dropdown-divider"></div>
            <Link  href="/requestList">
                <a className="dropdown-item"> Request List</a>
            </Link>
            </>
        )
    }

    const managerRouter = () => {
        return(
            <>
            <div className="dropdown-divider"></div>
            <Link href="/manager">
                <a className="dropdown-item">Manager</a>
            </Link>
            </>
        )
    }

    const hrRouter = () => {
        return(
            <>
            <div className="dropdown-divider"></div>
            <Link href="/hr">
                <a className="dropdown-item">HR</a>
            </Link>
            </>
        )
    }

    const loggedRouter = () => {
        return(
            <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src={auth.user.avatar} alt={auth.user.avatar} 
                    style={{
                        borderRadius: '50%', width: '30px', height: '30px',
                        transform: 'translateY(-8px)', marginRight: '3px', marginBottom: '-12px'
                    }} /> {auth.user.name}
                </a>

                <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <Link href="/profile">
                        <a className="dropdown-item">Profile</a>
                    </Link>

                    {
                        auth.user.role === 'admin' && adminRouter()
                    }

                    {
                        auth.user.position === 'employee' && userRouter()
                    }

                    {
                        auth.user.position === 'manager' && managerRouter()
                    }

                    {
                        auth.user.position === 'hr' && hrRouter()
                    }

                    <div className="dropdown-divider"></div>
                    <Link href="/">
                        <a className="dropdown-item" onClick={handleLogout}>Logout</a>
                    </Link>
                </div>
            </li>
        )
    }

    return (
        <div className="">
            <nav className="navbar navbar-expand-lg" style={{backgroundColor:'lightsteelblue', color:'white'}}>
                <Link  href="/">
                    <a className="navbar-brand" style={{fontFamily: 'lucida,sans-serif'}}>LEGIONTECH INC</a>
                </Link>
                <button className="navbar-toggler navbar-light" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon "></span>
                </button>
                <div className="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link  href="/">
                                <a className={"nav-link" + isActive('/')}>
                                    <i className="fa fa-home" aria-hidden="true"></i> Home
                                </a>
                            </Link>
                        </li>

                        {
                            Object.keys(auth).length === 0 
                            ? <li className="nav-item">
                                <Link href="/signin">
                                    <a className={"nav-link" + isActive('/signin')}>
                                        <i className="fas fa-user" aria-hidden="true"></i> Sign in
                                    </a>
                                </Link>
                        </li>
                        : loggedRouter()
                    }
                </ul>
            </div>
        </nav>
      </div>
    )
}

export default NavBar