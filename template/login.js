var usernameArray=[];
var passwordArray=[];

var loginBox = document.getElementById("login");
var regBox = document.getElementById("register");
var forgetBox = document.getElementById("forgot");

var loginTab = document.getElementById("lt");
var regTab = document.getElementById("rt");

function regTabFun(){
    event.preventDefault();

    regBox.style.visibility="visible";
    loginBox.style.visibility="hidden";
    forgetBox.style.visibility="hidden";

    loginTab.style.opacity="";
    regTab.style.opacity="";

}
function loginTabFun(){
    event.preventDefault();

    regBox.style.visibility="hidden";
    loginBox.style.visibility="visible";
    forgetBox.style.visibility="hidden";

    regTab.style.opacity="";
    loginTab.style.opacity="";

}
function forTabFun(){
    event.preventDefault();

    regBox.style.visibility="hidden";
    loginBox.style.visibility="hidden";
    forgetBox.style.visibility="visible";

    regTab.style.backgroundColor="#3a396e";
    loginTab.style.backgroundColor="#3a396e";

}


function register(){
    event.preventDefault();

    var email = document.getElementById("re").value;
    var username = document.getElementById("ru").value;
    var password = document.getElementById("rp").value;
    var passwordRetype = document.getElementById("rrp").value;

    if (email == ""){
        alert("Email is required.");
        return ;
    }
    else if (username == ""){
        alert("Username is required.");
        return ;
    }
    else if (password == ""){
        alert("Password is required.");
        return ;
    }
    else if (passwordRetype == ""){
        alert("Please re-type your password.");
        return ;
    }
    else if ( password != passwordRetype ){
        alert("Password doesn't match, please retype your Password.");
        return;
    }
    else if(usernameArray.indexOf(username) == -1){
        usernameArray.push(username);
        passwordArray.push(password);

        alert("Thanks for registration, "+ username + "!☺ \nYou may go to Login now");

        document.getElementById("re").value ="";
        document.getElementById("ru").value ="";
        document.getElementById("rp").value="";
        document.getElementById("rrp").value="";
    }
    else{
        alert("The email you use is already have an account. \n Please use other email.");
        return ;
    }
}
function login(){
    event.preventDefault();

    var username = document.getElementById("su").value;
    var password = document.getElementById("sp").value;

    var i = usernameArray.indexOf(username);

    if(usernameArray.indexOf(username) == -1){
        if (username == ""){
            alert("Username is required.");
            return ;
        }
        alert("Account does not exist. Please register first!");
        return ;
    }
    else if(passwordArray[i] != password){
        if (password == ""){
            alert("Password required.");
            return ;
        }
        alert("Password does not match.");
        return ;
    }
    else {
        alert("Login Success! \nWelcome to our website, " + username + "☺");

        document.getElementById("su").value ="";
        document.getElementById("sp").value="";
        return ;
    }

}

function forgot(){
    event.preventDefault();

    var email = document.getElementById("fe").value;

    if(emailArray.indexOf(email) == -1){
        if (email == ""){
            alert("Email required.");
            return ;
        }
        alert("Email does not exist.");
        return ;
    }

    alert("Email was sent to your account. Please check it in 24hrs. \n Thank you!");
    document.getElementById("fe").value ="";
}


